import React, { Component } from 'react'
import { connect } from 'react-redux'
import { giamSoLuongAction, tangSoLuongAction } from './redux/actions/numberAction';


class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div>
        <h3 className="display-4 text-success">{this.props.number}</h3>
        <div>
        <button onClick={this.props.tangSoLuong} className="btn btn-success">Tăng số lượng</button>
        <span> </span>
        <button onClick={()=>{this.props.giamSoLuong(1)}} className="btn btn-danger">Giảm số lượng</button>
        </div>

      </div>
    )
  }
};

const mapStateToProps = (state)=> {
    return {
        number: state.numberReducer.number,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        tangSoLuong:() => {
            dispatch(tangSoLuongAction());
        },
        giamSoLuong:(soLuong) => {
            dispatch(giamSoLuongAction(soLuong));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);

